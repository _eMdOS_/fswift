import Foundation

public enum Laws<Element: Equatable> {}

public extension Laws {
    static func isUnital(identity: Element, a: Element, operation: (Element, Element) -> Element) -> Bool {
        operation(a, identity) == a && operation(identity, a) == a
    }

    static func isAssociative(a: Element, b: Element, c: Element, operation: (Element, Element) -> Element) -> Bool {
        operation(operation(a, b), c) == operation(a, operation(b, c))
    }

    static func isIsomorphisma<A>(a: Element, direct: @escaping (Element) -> A, inverse: @escaping (A) -> Element) -> Bool {
        inverse(direct(a)) == a
    }
}
