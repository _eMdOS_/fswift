import Foundation

/**
 # Monoid

 In abstract algebra, a Monoid is an algebraic structure with a single *associative binary operation* and an *identity element*, in other words,
 Monoids are Semigroups with identity.

 When a Monoid is seen as a Magma, we can say that it is *associative* and *unital*.

 The identity element in a Monoid, acts as "empty" (*neutral*) in respect to the operation.

 Laws:

    + associative: `(a <> b) <> c = a <> (b <> c)`
    + unital: `id <> a = a = a <> id`
 */
public protocol Monoid: Semigroup {
    static var empty: Self { get }
}

public func concat<M: Monoid>(_ set: [M]) -> M {
    concan(M.empty, set)
}
