import Foundation
import Operators

/**
 # Magma

 A Magma (or binary algebraic structure) is a set equipped with a **binary operation** on it, it means, an operation that takes two values of a particular type, and
 returns a value of the same type.

 The magma composition is commonly expressed by using the "diamond operator" (`<>`).

 A magma is called:

    + **unital** if it has a neutral element (identity element):

        `id <> a = a = a <> id`

    + **commutative** if the binary operation takes the same value when its two arguments are interchanged:

        `a <> b = b <> a`

    + **associative** if the binary operation satisfies the associativity law (which is a semigroup):

        `(a <> b) <> c = a <> (b <> c)`
 */
public protocol Magma {
    static func <> (left: Self, right: Self) -> Self
}
