import Foundation
import Operators

/**
 # Semigroup

 A Semigroup is a Magma where the composition operation is *associative*:

    `(a <> b) <> c = a <> (b <> c)`

 It is like a Monoid where there might not be an identity element, which is the same to say that the Magma is *associative* but *not unital*.
 */
public protocol Semigroup: Magma {}

public func concan<S: Semigroup>(_ initial: S, _ set: [S]) -> S {
    set.reduce(initial, <>)
}
