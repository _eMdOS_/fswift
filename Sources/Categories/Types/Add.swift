import Foundation
import Operators

public protocol Addable {
    static var zeroIdentity: Self { get }
    static func add(left: Self, right: Self) -> Self
}

public struct Add<A: Addable>: Wrapper {
    public typealias Wrapped = A
    public let unwrap: A
    public init(_ value: A) {
        unwrap = value
    }
}

extension Add: Semigroup {
    public static func <> (left: Add, right: Add) -> Add {
        Add(A.add(left: left.unwrap, right: right.unwrap))
    }
}

extension Add: Monoid {
    public static var empty: Add { Add(A.zeroIdentity) }
}

extension Add: Equatable where Wrapped: Equatable {}

// MARK: Swift types

extension Int: Addable {
    public static let zeroIdentity: Int = 0

    public static func add(left: Int, right: Int) -> Int {
        switch (left, right) {
        case (Int.max, _), (_, Int.max):
            return Int.max
        default:
            return left + right
        }
    }
}

extension UInt: Addable {
    public static let zeroIdentity: UInt = 0

    public static func add(left: UInt, right: UInt) -> UInt {
        switch (left, right) {
        case (UInt.max, _), (_, UInt.max):
            return UInt.max
        default:
            return left + right
        }
    }
}

extension Double: Addable {
    public static let zeroIdentity: Double = 0.0

    public static func add(left: Double, right: Double) -> Double {
        left + right
    }
}

extension Float: Addable {
    public static let zeroIdentity: Float = 0.0

    public static func add(left: Float, right: Float) -> Float {
        left + right
    }
}
