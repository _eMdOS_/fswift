import Foundation
import Operators

extension String: Semigroup {
    public static func <> (left: String, right: String) -> String {
        left + right
    }
}

extension String: Monoid {
    public static let empty: String = ""
}
