import Foundation

/**
 # Wrapper

 Protocol definition for a type that wraps another.

 When retrieving the `wrapped` value to construct another `Wrapper` type, it ends up with the build of the same `Wrapper` type.
 This is an example of an **Isomorphism**.
 */
public protocol Wrapper {
    associatedtype Wrapped
    init(_ value: Wrapped)
    var unwrap: Wrapped { get }
}

extension Wrapper where Wrapped: Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.unwrap == rhs.unwrap
    }
}
