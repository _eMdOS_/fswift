import Foundation
import Operators

extension Array: Semigroup {
    public static func <> (left: Array, right: Array) -> Array {
        left + right
    }
}

extension Array: Monoid {
    public static var empty: Array { [] }
}
