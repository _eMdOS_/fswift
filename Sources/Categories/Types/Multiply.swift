import Foundation
import Operators

public protocol Multipliable {
    static var oneIdentity: Self { get }
    static func multiply(left: Self, right: Self) -> Self
}

public struct Multiply<M: Multipliable>: Wrapper {
    public typealias Wrapped = M
    public let unwrap: M
    public init(_ value: M) {
        unwrap = value
    }
}

extension Multiply: Semigroup {
    public static func <> (left: Multiply, right: Multiply) -> Multiply {
        Multiply(M.multiply(left: left.unwrap, right: right.unwrap))
    }
}

extension Multiply: Monoid {
    public static var empty: Multiply {
        Multiply(M.oneIdentity)
    }
}

extension Multiply: Equatable where Wrapped: Equatable {}

// MARK: Swift types

extension Int: Multipliable {
    public static let oneIdentity: Int = 1

    public static func multiply(left: Int, right: Int) -> Int {
        left * right
    }
}

extension UInt: Multipliable {
    public static let oneIdentity: UInt = 1

    public static func multiply(left: UInt, right: UInt) -> UInt {
        left * right
    }
}

extension Double: Multipliable {
    public static let oneIdentity: Double = 1.0

    public static func multiply(left: Double, right: Double) -> Double {
        left * right
    }
}

extension Float: Multipliable {
    public static let oneIdentity: Float = 1.0

    public static func multiply(left: Float, right: Float) -> Float {
        left * right
    }
}
