// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "fswift",
    platforms: [
        .iOS(.v13),
        .tvOS(.v13),
        .macOS(.v10_15)
    ],
    products: [
        .library(
            name: "fswift",
            targets: ["Categories"]
        ),
    ],
    dependencies: [],
    targets: [
        // Operators
        .target(name: "Operators"),
        // Categories
        .target(name: "Categories", dependencies: ["Operators"]),
        .testTarget(name: "CategoriesTests", dependencies: ["Categories", "Operators"])
    ],
    swiftLanguageVersions: [.v5]
)
