import XCTest
import Operators
import Categories

final class MonoidTests: XCTestCase {
    func testString() {
        XCTAssert(Laws<String>.isUnital(identity: .empty, a: "a", operation: <>))
    }

    func testArray() {
        XCTAssert(Laws<[String]>.isUnital(identity: .empty, a: ["a"], operation: <>))
    }

    func testAdd() {
        // Add<Int>
        XCTAssert(Laws<Add<Int>>.isUnital(identity: .empty, a: Add(2), operation: <>))
        // Add<UInt>
        XCTAssert(Laws<Add<UInt>>.isUnital(identity: .empty, a: Add(2), operation: <>))
        // Add<Double>
        XCTAssert(Laws<Add<Double>>.isUnital(identity: .empty, a: Add(2), operation: <>))
        // Add<Float>
        XCTAssert(Laws<Add<Float>>.isUnital(identity: .empty, a: Add(2), operation: <>))
    }

    func testMultiply() {
        // Multiply<Int>
        XCTAssert(Laws<Multiply<Int>>.isUnital(identity: .empty, a: Multiply(2), operation: <>))
        // Multiply<UInt>
        XCTAssert(Laws<Multiply<UInt>>.isUnital(identity: .empty, a: Multiply(2), operation: <>))
        // Multiply<Double>
        XCTAssert(Laws<Multiply<Double>>.isUnital(identity: .empty, a: Multiply(2), operation: <>))
        // Multiply<Float>
        XCTAssert(Laws<Multiply<Float>>.isUnital(identity: .empty, a: Multiply(2), operation: <>))
    }
}
