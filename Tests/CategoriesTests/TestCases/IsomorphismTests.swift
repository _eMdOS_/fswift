import XCTest
import Operators
import Categories

final class IsomorphismTests: XCTestCase {
    func testAdd() {
        // Add<Int>
        XCTAssert(Laws<Int>.isIsomorphisma(a: 2, direct: Add.init, inverse: { $0.unwrap }))
        // Add<UInt>
        XCTAssert(Laws<UInt>.isIsomorphisma(a: 2, direct: Add.init, inverse: { $0.unwrap }))
        // Add<Double>
        XCTAssert(Laws<Double>.isIsomorphisma(a: 2, direct: Add.init, inverse: { $0.unwrap }))
        // Add<Float>
        XCTAssert(Laws<Float>.isIsomorphisma(a: 2, direct: Add.init, inverse: { $0.unwrap }))
    }

    func testMultiply() {
        // Multiply<Int>
        XCTAssert(Laws<Int>.isIsomorphisma(a: 2, direct: Multiply.init, inverse: { $0.unwrap }))
        // Multiply<UInt>
        XCTAssert(Laws<UInt>.isIsomorphisma(a: 2, direct: Multiply.init, inverse: { $0.unwrap }))
        // Multiply<Double>
        XCTAssert(Laws<Double>.isIsomorphisma(a: 2, direct: Multiply.init, inverse: { $0.unwrap }))
        // Multiply<Float>
        XCTAssert(Laws<Float>.isIsomorphisma(a: 2, direct: Multiply.init, inverse: { $0.unwrap }))
    }
}
