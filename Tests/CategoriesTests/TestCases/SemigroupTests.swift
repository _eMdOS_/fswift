import XCTest
import Operators
import Categories

final class SemigroupTests: XCTestCase {
    func testString() {
        XCTAssert(Laws<String>.isAssociative(a: "a", b: "b", c: "c", operation: <>))
    }

    func testArray() {
        XCTAssert(Laws<[String]>.isAssociative(a: ["a"], b: ["b"], c: ["c"], operation: <>))
    }

    func testAdd() {
        // Add<Int>
        XCTAssert(Laws<Add<Int>>.isAssociative(a: Add(1), b: Add(2), c: Add(3), operation: <>))
        // Add<UInt>
        XCTAssert(Laws<Add<UInt>>.isAssociative(a: Add(1), b: Add(2), c: Add(3), operation: <>))
        // Add<Double>
        XCTAssert(Laws<Add<Double>>.isAssociative(a: Add(1), b: Add(2), c: Add(3), operation: <>))
        // Add<Float>
        XCTAssert(Laws<Add<Float>>.isAssociative(a: Add(1), b: Add(2), c: Add(3), operation: <>))
    }

    func testMultiply() {
        // Multiply<Int>
        XCTAssert(Laws<Multiply<Int>>.isAssociative(a: Multiply(1), b: Multiply(2), c: Multiply(3), operation: <>))
        // Multiply<UInt>
        XCTAssert(Laws<Multiply<UInt>>.isAssociative(a: Multiply(1), b: Multiply(2), c: Multiply(3), operation: <>))
        // Multiply<Double>
        XCTAssert(Laws<Multiply<Double>>.isAssociative(a: Multiply(1), b: Multiply(2), c: Multiply(3), operation: <>))
        // Multiply<Float>
        XCTAssert(Laws<Multiply<Float>>.isAssociative(a: Multiply(1), b: Multiply(2), c: Multiply(3), operation: <>))
    }
}
